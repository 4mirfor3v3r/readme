<div align="center">
<img width="150" height="150" src="https://gitlab.com/pt-dot-internal/taprose-android/raw/develop/app/src/main/ic_launcher-web.png" />
</div>
<div align="center">
    <a href="https://android-arsenal.com/api?level=19" alt="API">
        <img src="https://img.shields.io/badge/API-19%2B-brightgreen.svg?style=flat" />
    </a>
    <a href="https://gitlab.com/pt-dot-internal/taprose-android/pipelines" alt="Build">
        <img src="https://gitlab.com/pt-dot-internal/taprose-android/badges/develop/pipeline.svg?job=Build&key_text=Build&key_width=50" />
    </a>
    <a href="https://gitlab.com/pt-dot-internal/taprose-android/pipelines" alt="Deploy">
        <img src="https://gitlab.com/pt-dot-internal/taprose-android/badges/develop/pipeline.svg?job=Deploy&key_text=Deploy&key_width=50" />
    </a>
</div>
<br/>
<div align="center">
<a href="https://play.google.com/store/apps/details?id=dot.com.taprose" alt="Playstore">
    <img src="https://taprose.tubankab.go.id/img/landing/google-play-badge.png" />
</a>
</div>

# TAPROSE
> TapRose adalah Aplikasi yang memungkinkan Anda melaporkan langsung ke Pemerintah Kabupaten, mereview jasa atau pelayanan para pihak swasta (tempat wisata, tempat makan, cafe, hotel atau tempat kebugaran), serta berbagi informasi dengan warga Tuban untuk menciptakan Smart City.

Table of contents
=================
<details>
<summary>Click to show</summary>

[[_TOC_]]

</details>

### Preview App
|           WELCOME                                                                 |           LOGIN                                                                   |               HOME-ADUAN                                                          |               HOME-OPD                                                            |                   SWASTA                                                          |                   FORUM                                                           |               PROFILE                                                             |
|:-:                                                                                |:-:                                                                                |:-:                                                                                |:-:                                                                                |:-:                                                                                |:-:                                                                                |:-:                                                                                |
|![](https://drive.google.com/uc?export=view&id=1GbbuMtbcDz1Hn-kj9bNIdZWqc8aDL08K)  | ![](https://drive.google.com/uc?export=view&id=1AvcR2h0hzqGinBWaD8SXkE_Cb2R_jD7U) | ![](https://drive.google.com/uc?export=view&id=1_t9OSIsfDfV-IV0VaX6aQIvipKtNPO_I) | ![](https://drive.google.com/uc?export=view&id=1qjJv4Jvt4Y8HUcPTOoYfzICAQ6Fc_L-U) | ![](https://drive.google.com/uc?export=view&id=1SN8XpkOuY4JoFHWOTC-TZsvXTA7YN4aa) | ![](https://drive.google.com/uc?export=view&id=1Yo5759KjHAGcY_-1WkFlFbybA303YaeK) | ![](https://drive.google.com/uc?export=view&id=1oVBXxECqXWNMrHIZ3K77qesMLXJVhCXa) |
|---                                                                                | ---                                                                               | ![](https://drive.google.com/uc?export=view&id=1itsVFZv2HY9liyMSMVUV-AEaYfBRJB0A) | ![](https://drive.google.com/uc?export=view&id=1lc5P5JaRolydhii8_ZLUjGiKdSUnI1hq) | ![](https://drive.google.com/uc?export=view&id=10IZRa-HVSSK6N3qgAYI1-Ft4jaHNAaoW) | ![](https://drive.google.com/uc?export=view&id=1vFZtMNx8v0j9TxM_EZoszkXqJev-LcLT) | ![](https://drive.google.com/uc?export=view&id=1-IBbhD9k4U2iq8imhtMCT5naY1c2nUxw) |
|---                                                                                | ---                                                                               | ![](https://drive.google.com/uc?export=view&id=1o21OKqn9_dvULGgHfVNrmngOAdh7DkYf) | ---                                                                               | ![](https://drive.google.com/uc?export=view&id=1k7gSJGgB360A1L8K0whd-LsdCkBoU4Gp) | ![](https://drive.google.com/uc?export=view&id=1qO1LkmbGh1QSCFF9uK-t2pwqpV6wsQXF) | ---                                                                               |

# Guidelines android developer
> Read this guideline first to start code 👉 [DOT-Guidelines](https://github.com/4mirfor3v3r/DOT-Android-Guidelines) 👈 open for improvement


## Naming CheatSheet

### Classes & Layouts
| Component        | Class Name                      | Layout Name                              |
| -----------------| --------------------------------| -----------------------------------------|
| Activity         | `MainBroadcastMessageActivity`  | `activity_main_broadcast_message.xml`    |
| Fragment         | `FragmentHome`                  | `fragment_home.xml`                      |
| Dialog           | ---                             | `dialog_rating.xml`                      |
| Adapter          | `ForumImageAdapter`             | ---                                      |
| AdapterView item | ---                             | `item_forum_image.xml`                   |
| Partial layout   | ---                             | `view_aduan_location.xml`                |

### Layout IDs
| Component                 | Prefix            | Example                       |
| --------------            | :------:          | :---------------------:       |
| Appbar                    | `appbar`          | `appbarHomeDetail`            |
| Bottom Navigation         | `botnav`          | `botNavMain`                  |
| Bottom Sheet              | `bs`              | `bsListLaporan`               |
| Button                    | `btn`             | `btnRegisterSignup`           |
| Checkbox                  | `cb`              | `cbLoginRememberMe`           |
| Collapse Toolbar Layout   | `ctl`             | `ctlHomeDetail`               |
| ConstraintLayout          | `cl`              | `clSplashRoot`                |
| CoordinatorLayout         | `cor`             | `corForumDetail`              |
| Custom Widget             | `wg`              | `wgMainGridRv`                |
| EditText                  | `et`              | `etLoginPassword`             |
| Fragment Location         | `_Frag_` (Affix)  | `llSwastaFragDetail`          |
| ImageView                 | `iv`              | `ivSplashLogo`                |
| Layout Binding ViewModel  |  ---              | `vm`                          |
| Library Layout            | `lib`             | `libMainBotNav`               |
| LinearLayout              | `ll`              | `llLoginRoot`                 |
| MapView                   | `mv`              | `mvSwastaFrag`                |
| Menu                      | `menu`            | `menuMainSearch`              |
| NestedScrollView          | `nsv`             | `nsvHomeDetail`               |
| ProgressBar               | `pb`              | `pbRegisterUploadPercent`     |
| RecyclerView              | `rv`              | `rvChat`                      |
| RelativeLayout            | `rl`              | `rlMainRoot`                  |
| TabLayout                 | `tab`             | `tabMainNav`                  |
| TextView                  | `tv`              | `tvWelcomeTitle`              |
| RadioButton               | `rb`              | `rbInputFemale`               |
| RadioGroup                | `rg`              | `rgInputGender`               |
| Spinner                   | `spin`            | `spinEditProfileLocation`     |
| Swipe Refresh Layout      | `srl`             | `srlForum`                    |
| View                      | `view`            | `viewForumListDivider`        |

## Package Structure

### JAVA
<div align="center">
<img src="https://drive.google.com/uc?export=view&id=1PaIpdd8aHy7zRvfcEx1smbxg_ht07vae" alt="struc-java" />
</div>


### Resource
#### Resource based on features and java packages (Still need improvements🧰)
```
res-feature
    │  
    ├─── auth (features name)
    |   |
    │   ├───drawables
    │   │    └─ Drawable directory inside package
    │   │
    │   ├───  layout
    │   │    └─ Layout inside package
    │   │
    │   └───  values
    │        └─ String inside package
    │   
    ├─── main (main package)
    │
    ├─── menu (menu package)
    │
    │─── widget (widget package) 
    |
    │───xdrawables
    │    └──  root drawable (used more than one time)
    │
    ├─── layout (layout that used multiple times i.e. partial layout)
    │
    └─── values (values used more than one time, tools i.e (lorem, image url), never used)
        ├───  attrs
        ├───  colors
        ├───  dimens
        ├───  other values
        ├───  strings
        └───  styles
```


# Library Used

* [AUTOSTART MANAGER](https://github.com/judemanutd/AutoStarter)  AUTOSTART MANAGER
* [EMOJI-IOS](https://github.com/vanniktech/Emoji) EMOJI KEYBOARD
* [ESPRESSO](https://developer.android.com/training/testing/espresso) AUTOMATION UI TESTING
* [FABRIC](https://firebase.google.com/docs/crashlytics) CRASHLITYCS
* [FLOWLAYOUT](https://github.com/ApmeM/android-flowlayout) Layout Addon
* [GEOCODER](https://github.com/Doctoror/Geocoder) LAT-LNG-Translator  --DEPRECATED
* [GLIDE](https://github.com/bumptech/glide) IMAGE CACHING
* [HASHTAGVIEW](https://github.com/greenfrvr/hashtag-view/tree/master/hashtag-view) Layout Addon
* [IMAGE COMPRESSOR](https://github.com/zetbaitsu/Compressor) COMPRESS IMAGE SIZE
* [INQUIRY](https://github.com/afollestad/inquiry-deprecated) INQUIRY DATABASE  --DEPRECATED
* [MULTIIMAGE-SELECTOR](https://github.com/lovetuzitong/MultiImageSelector/tree/master/multi-image-selector) Image Picker
* [ONESIGNAL](https://documentation.onesignal.com/docs/android-sdk-setup) PUSH NOTIFICATION
* [PARCELER](http://parceler.org/) TRANSFER OBJECT CLASS AS PARCEL
* [PHOTOVIEW](https://github.com/chrisbanes/PhotoView) Layout Addon
* [PRETTYTIME](https://github.com/ocpsoft/prettytime) Time and Date Formatter
* [REACTIVEX](https://github.com/ReactiveX/RxJava) Reactive Programming
* [RESERVOIR](https://github.com/anupcowkur/Reservoir) Caching object by Key-Value
* [RETROFIT](https://github.com/square/retrofit) Networking
* [SENTRY](https://docs.sentry.io/platforms/android/) CRASHLITYCS
* [SICLO EZPHOTOPICK](https://github.com/Siclo-Mobile/EZPhotoPicker) IMAGE Picker
* [SLF4J](http://www.slf4j.org/) Java Logger
* [SOTHREE SLIDING UP PANEL](https://github.com/umano/AndroidSlidingUpPanel) Layout Addon
* [TAJCHERT NAMMU](https://github.com/tajchert/Nammu) Permission Helper
* [TOASTY](https://github.com/GrenderG/Toasty) Beautiful Toast Library


----
# License
[MIT](https://choosealicense.com/licenses/mit/)
